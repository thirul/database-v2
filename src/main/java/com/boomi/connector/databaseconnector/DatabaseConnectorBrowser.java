// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.databaseconnector;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.api.OperationType;
import com.boomi.connector.databaseconnector.util.DatabaseConnectorConstants;
import com.boomi.connector.databaseconnector.util.ProcedureMetaDataUtil;
import com.boomi.connector.databaseconnector.util.SchemaBuilderUtil;
import com.boomi.connector.util.BaseBrowser;

/**
 * The Class DatabaseConnectorBrowser.
 *
 * @author swastik.vn
 */
public class DatabaseConnectorBrowser extends BaseBrowser implements ConnectionTester {

	/**
	 * Instantiates a new database connector browser.
	 *
	 * @param conn the conn
	 */
	public DatabaseConnectorBrowser(DatabaseConnectorConnection conn) {
		super(conn);
	}

	/** The Constant logger. */
	private static final Logger logger = Logger.getLogger(DatabaseConnectorBrowser.class.getName());

	/**
	 * Gets the object definitions.
	 *
	 * @param objectTypeId the object type id
	 * @param roles        the roles
	 * @return the object definitions
	 */
	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {

		String customOpsType = getContext().getCustomOperationType();
		if (customOpsType==null)
			customOpsType=""; //avoid switch npe
		OperationType opsType = getContext().getOperationType();
		String getType = (String) getContext().getOperationProperties().get(DatabaseConnectorConstants.GET_TYPE);
		String updateType = (String) getContext().getOperationProperties().get(DatabaseConnectorConstants.TYPE);
		String deleteType = (String) getContext().getOperationProperties().get(DatabaseConnectorConstants.DELETE_TYPE);
		String insertType = (String) getContext().getOperationProperties()
				.get(DatabaseConnectorConstants.INSERTION_TYPE);
		boolean enableQuery = getContext().getOperationProperties().getBooleanProperty("enableQuery", false);
		ObjectDefinitions objdefs = new ObjectDefinitions();
		DatabaseConnectorConnection conn = getConnection();
		try (Connection con = conn.connect()) {
			for (ObjectDefinitionRole role : roles) {
				ObjectDefinition objdef = new ObjectDefinition();
				String jsonSchema = null;
				switch (role) {
				case OUTPUT:
					switch (customOpsType)
					{
						case DatabaseConnectorConstants.STOREDPROCEDUREWRITE:
							List<String> outParams = ProcedureMetaDataUtil.getOutputParams(con, objectTypeId);
							jsonSchema = SchemaBuilderUtil.getProcedureSchema(con, objectTypeId, outParams);
							break;
						case DatabaseConnectorConstants.GET:
							jsonSchema = SchemaBuilderUtil.getJsonSchema(con, objectTypeId, false, true);
							break;
						case DatabaseConnectorConstants.BEGIN_TRANSACTION:
						case DatabaseConnectorConstants.COMMIT:
						case DatabaseConnectorConstants.ROLLBACK:
							jsonSchema = null;
							break;
						default:
							jsonSchema = SchemaBuilderUtil.getQueryJsonSchema("");
					}
					if (jsonSchema == null) {
						objdefs = this.getUnstructuredSchema(objdef, objdefs);
					} else {
						objdefs = this.getJsonStructure(jsonSchema, objdef, objdefs);
					}

					break;

				case INPUT:
					if (DatabaseConnectorConstants.STOREDPROCEDUREWRITE.equals(customOpsType)) {
						List<String> inParams = ProcedureMetaDataUtil.getInputParams(con, objectTypeId);
						jsonSchema = SchemaBuilderUtil.getProcedureSchema(con, objectTypeId, inParams);
					} else if (DatabaseConnectorConstants.DYNAMIC_UPDATE.equals(updateType)) {
						jsonSchema = SchemaBuilderUtil.getQueryJsonSchema(updateType);
					} else if (DatabaseConnectorConstants.DYNAMIC_DELETE.equals(deleteType)) {
						jsonSchema = SchemaBuilderUtil.getQueryJsonSchema(deleteType);
					} else if (DatabaseConnectorConstants.DYNAMIC_INSERT.equals(insertType)
							|| DatabaseConnectorConstants.DYNAMIC_GET.equals(getType)
							|| OperationType.UPSERT.equals(opsType)) {
						jsonSchema = SchemaBuilderUtil.getJsonSchema(con, objectTypeId, false, false);
					} else if (DatabaseConnectorConstants.BEGIN_TRANSACTION.contentEquals(customOpsType) || DatabaseConnectorConstants.COMMIT.contentEquals(customOpsType) || DatabaseConnectorConstants.ROLLBACK.contentEquals(customOpsType)) {
						jsonSchema=null;
					}					
					else {
						jsonSchema = SchemaBuilderUtil.getJsonSchema(con, objectTypeId, enableQuery, false);
					}
					if (jsonSchema != null) {
						objdefs = this.getJsonStructure(jsonSchema, objdef, objdefs);
					} else {
						objdefs = this.getUnstructuredSchema(objdef, objdefs);
					}
					break;
				default:
					break;
				}
			}
		} catch (SQLException e) {
			throw new ConnectorException(e.getMessage());
		}

		return objdefs;
	}

	/**
	 * Method that will take {@link ObjectDefinition} and build the unstructured
	 * Schema for request and response profile.
	 *
	 * @param objdef  the objdef
	 * @param objdefs the objdefs
	 * @return objdefs
	 */
	private ObjectDefinitions getUnstructuredSchema(ObjectDefinition objdef, ObjectDefinitions objdefs) {
		objdef.setElementName("");
		objdef.setOutputType(ContentType.BINARY); //TODO it is not best practice to set but input and output types for a INPUT or OUTPUT role (davehock)
		objdef.setInputType(ContentType.NONE);
		objdefs.getDefinitions().add(objdef);
		return objdefs;
	}

	/**
	 * This method will take {@link ObjectDefinition} and jsonSchema and it will
	 * build the structured Schema for request and response profile.
	 *
	 * @param jsonSchema the json schema
	 * @param objdef     the objdef
	 * @param objdefs    the objdefs
	 * @return objdefs
	 */
	private ObjectDefinitions getJsonStructure(String jsonSchema, ObjectDefinition objdef, ObjectDefinitions objdefs) {
		objdef.setElementName("");
		objdef.setJsonSchema(jsonSchema);
		objdef.setOutputType(ContentType.JSON); //TODO it is not best practice to set but input and output types for a INPUT or OUTPUT role (davehock)
		objdef.setInputType(ContentType.JSON);
		objdefs.getDefinitions().add(objdef);
		return objdefs;
	}

	/**
	 * This method will add the table names or the procedure names to the object
	 * type list based on the operation selected.
	 *
	 * @return the object types
	 */
	@Override
	public ObjectTypes getObjectTypes() {

		ObjectTypes objtypes = new ObjectTypes();
		List<ObjectType> objTypeList = new ArrayList<>();
		DatabaseConnectorConnection conn = getConnection();
		ResultSet resultSet = null;
		ObjectType objectType = null;
		try (Connection con = conn.connect()) {
			DatabaseMetaData md = con.getMetaData();
			String opsType = getContext().getCustomOperationType();
			if (opsType==null)
				opsType=""; //avoid switch npe
			switch (opsType)
			{
				case DatabaseConnectorConstants.STOREDPROCEDUREWRITE:
					resultSet = md.getProcedures(null, con.getSchema(), "%");
					while (resultSet.next()) {
						String procedureName = resultSet.getString(DatabaseConnectorConstants.PROCEDURE_NAME);
						if (md.getDatabaseProductName().equals(DatabaseConnectorConstants.MSSQLSERVER)) {
							objectType = new ObjectType();
							objectType.setId(procedureName.substring(0, procedureName.length() - 2));
							objTypeList.add(objectType);
						} else {
							objectType = new ObjectType();
							objectType.setId(procedureName);
							objTypeList.add(objectType);
						}
					}
					break;
				case DatabaseConnectorConstants.BEGIN_TRANSACTION:
				case DatabaseConnectorConstants.COMMIT:
				case DatabaseConnectorConstants.ROLLBACK:
					objectType = new ObjectType();
					objectType.setId(opsType);
					objectType.setLabel(opsType.replace("_", " "));
					objTypeList.add(objectType);
					break;
				default:
					String tableNames = getContext().getOperationProperties().getProperty("tableNames", null);
					if (tableNames != null) {
						objectType = this.validate(tableNames, con);
						objTypeList.add(objectType);
					} else {
						resultSet = md.getTables(null, con.getSchema(), null,
								new String[] { DatabaseConnectorConstants.TABLE });
						while (resultSet.next()) {
							objectType = new ObjectType();
							objectType.setId(resultSet.getString(DatabaseConnectorConstants.TABLE_NAME));
							objTypeList.add(objectType);
						}
					}
			}
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Unable to get the table names from the database {0}", e.getMessage());
			e.printStackTrace();
			throw new ConnectorException(e);
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					logger.log(Level.SEVERE, "Result set not closed properly!!");
				}
			}
		}
		objtypes.getTypes().addAll(objTypeList);
		return objtypes;
	}

	/**
	 * This method is to validate whether the table names provided by user exists in
	 * the database by Querying the particular table and fetching the 1st row if
	 * record exists.
	 *
	 * @param tableNames the table names
	 * @param con        the con
	 * @return the object type
	 * @throws SQLException
	 */
	private ObjectType validate(String tableNames, Connection con) throws SQLException {
		String[] tableName = tableNames.split("[,]", 0);
		ObjectType objectType = new ObjectType();
		for (String table : tableName) {
			try (PreparedStatement pstmnt = con.prepareStatement("SELECT count(*) FROM " + table.trim())) {
				pstmnt.setMaxRows(1);
				try (ResultSet rs = pstmnt.executeQuery()) {
					logger.fine(table + "exists!!");
				}
			} catch (SQLException e) {
				throw new ConnectorException(e.getMessage());
			}
		}
		if (con.getMetaData().getDatabaseProductName().equals("Oracle")) {
			objectType.setId(tableNames.toUpperCase());
		} else {
			objectType.setId(tableNames);
		}

		return objectType;

	}

	/**
	 * Gets Connection Object.
	 *
	 * @return the connection
	 */
	@Override
	public DatabaseConnectorConnection getConnection() {
		return (DatabaseConnectorConnection) super.getConnection();
	}

	/**
	 * Method to test the database Connection by taking connection parameters.
	 */
	@Override
	public void testConnection() {
		getConnection().test();
	}
}