// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.databaseconnector.util;

import java.io.IOException;
import java.io.OutputStream;
import java.net.ConnectException;
import java.sql.ResultSet;
import java.sql.SQLException;

import static com.boomi.connector.databaseconnector.util.DatabaseConnectorConstants.*;

import com.boomi.connector.api.BasePayload;
import com.boomi.connector.api.ConnectorException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;

/**
 * @author swastik.vn
 **/
public class CustomPayloadUtil extends BasePayload {

	/** The Constant JSON_FACTORY. */
	private static final JsonFactory JSON_FACTORY = new JsonFactory();

	/** The rs. */
	private ResultSet rs;

	/** The generator. */
	JsonGenerator generator = null;
	
	/**
	 * Creates a new instance. Closing the payload will close the Resultset
	 *
	 * @param resultset the resultset
	 */

	public CustomPayloadUtil(ResultSet resultset) {
		this.rs = resultset;
	}
	

	/**
	 * This method will write the resultset in Json using JsonGenerator to Output
	 * stream.
	 *
	 * @param out OutputStream
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void writeTo(OutputStream out) throws IOException {
		generator = JSON_FACTORY.createGenerator(out);
		try {
			generator.writeStartObject();

			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
				if (rs.getMetaData().getColumnType(i) == 4 || rs.getMetaData().getColumnType(i) == 2) {
					int value = rs.getInt(rs.getMetaData().getColumnLabel(i));
					generator.writeNumberField(rs.getMetaData().getColumnLabel(i), value);
					generator.flush();
				}
				else if (rs.getMetaData().getColumnType(i) == 12 || rs.getMetaData().getColumnType(i) == 91
						|| rs.getMetaData().getColumnType(i) == 92 || rs.getMetaData().getColumnType(i) == -1 
						|| rs.getMetaData().getColumnType(i) == 2005 || rs.getMetaData().getColumnType(i) == 93 || rs.getMetaData().getColumnType(i) == -9 || rs.getMetaData().getColumnTypeName(i).equalsIgnoreCase(JSON)) {
					String varchar = rs.getString(rs.getMetaData().getColumnLabel(i));
					generator.writeStringField(rs.getMetaData().getColumnLabel(i), varchar);
					generator.flush();
				}
				else if (rs.getMetaData().getColumnType(i) == 16 || rs.getMetaData().getColumnType(i) == -7) {
					boolean flag = rs.getBoolean(rs.getMetaData().getColumnLabel(i));
					generator.writeBooleanField(rs.getMetaData().getColumnLabel(i), flag);
					generator.flush();
				}else if(rs.getMetaData().getColumnType(i) == 2004) {
					generator.writeStringField(rs.getMetaData().getColumnLabel(i), new String(rs.getBytes(rs.getMetaData().getColumnLabel(i))));
					generator.flush();
				}
			}
			generator.writeEndObject();
			generator.flush();
		} catch (SQLException e) {
			throw new ConnectException(e.getMessage());
		} finally {
			generator.close();
		}
	}

	/**
	 * Close.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@Override
	public void close() throws IOException {
		try {
			if (!generator.isClosed()) {
				generator.close();
			}
		} catch (Exception e) {
			throw new ConnectorException(e.getMessage());
		}
	}

}
