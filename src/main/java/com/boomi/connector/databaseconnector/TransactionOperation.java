package com.boomi.connector.databaseconnector;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.api.UpdateRequest;
import com.boomi.connector.databaseconnector.util.DatabaseConnectorConstants;
import com.boomi.connector.util.BaseUpdateOperation;

/**
 * The Class StandardOperation.
 *
 * @author davehock
 */public class TransactionOperation extends BaseUpdateOperation {

	public TransactionOperation(DatabaseConnectorConnection connection) {
		super(connection);
	}
	
	@Override
	protected void executeUpdate(UpdateRequest request, OperationResponse response) {
		getConnection().setLogger(response.getLogger());
		String transactionOperation = getContext().getCustomOperationType();
		Logger logger = response.getLogger();
		logger.log(Level.INFO, transactionOperation);
		switch (transactionOperation)
		{
			case DatabaseConnectorConstants.BEGIN_TRANSACTION:
				this.getConnection().beginTransactionOperation();
				break;
			case DatabaseConnectorConstants.COMMIT:
				this.getConnection().commitOperation();
				break;
			case DatabaseConnectorConstants.ROLLBACK:
				this.getConnection().rollbackOperation();
				break;
		}
		ResponseUtil.addEmptySuccesses(response, request, "OK");
	}
	
	/**
	 * Gets the Connection instance.
	 *
	 * @return the connection
	 */
	@Override
	public DatabaseConnectorConnection getConnection() {
		return (DatabaseConnectorConnection) super.getConnection();
	}
}
