// Copyright (c) 2020 Boomi, Inc.
package com.boomi.connector.databaseconnector;

import java.sql.Connection;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.BrowseContext;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.databaseconnector.util.DatabaseConnectorConstants;
import com.boomi.connector.util.BaseConnection;
import com.boomi.util.StringUtil;
import com.boomi.execution.ExecutionManager;


/**
 * The Class DatabaseConnectorConnection.
 *
 * @author swastik.vn
 */
public class DatabaseConnectorConnection extends BaseConnection {

	/** The class name. */
	private String className;

	/** The username. */
	private String username;

	/** The password. */
	private String password;

	/** The url. */
	private String url;

	/** The custom property. */
	private Map<String, String> customProperty;

	private TransactableConnection _dbConnection;

	/** The Constant logger. */
	private Logger logger = Logger.getLogger(DatabaseConnectorConnection.class.getName());

	/**
	 * Instantiates a new database connector connection.
	 *
	 * @param context the context
	 */
	public DatabaseConnectorConnection(BrowseContext context) {
		super(context);
		this.className = getContext().getConnectionProperties().getProperty(DatabaseConnectorConstants.CLASSNAME,"");
		this.username = getContext().getConnectionProperties().getProperty(DatabaseConnectorConstants.USERNAME,"");
		this.password = getContext().getConnectionProperties().getProperty(DatabaseConnectorConstants.PASS,"");
		this.url = getContext().getConnectionProperties().getProperty(DatabaseConnectorConstants.URL,"");
		this.customProperty = getContext().getConnectionProperties().getCustomProperties("CustomProperties");
	}
	
	public void setLogger(Logger logger)
	{
		this.logger = logger;
	}

	/**
	 * Gets the class name.
	 *
	 * @return the class name
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * Sets the class name.
	 *
	 * @param className the new class name
	 */
	public void setClassName(String className) {
		this.className = className;
	}

	/**
	 * Gets the username.
	 *
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Sets the username.
	 *
	 * @param username the new username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Gets the url.
	 *
	 * @return the url
	 */
	public String getUrl() {
		if(StringUtil.isEmpty(url) || StringUtil.isBlank(url)) {
			throw new ConnectorException("The Connection URL field cannot be empty!!");
		}
		return url;
	}

	/**
	 * Sets the url.
	 *
	 * @param url the new url
	 *
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * Load properties.
	 *
	 * @return the properties
	 */
	public Properties loadProperties() {
		Properties prop = new Properties();
		prop.put("user", getUsername());
		prop.put("password", getPassword());
		if (!customProperty.isEmpty()) {
			for (Map.Entry<String, String> entry : customProperty.entrySet()) {
				prop.put(entry.getKey(), entry.getValue());
			}
		}

		return prop;

	}

	/**
	 * Gets the solo connection.
	 *
	 * @return the solo connection
	 */
	public Driver getSoloConnection() {

		try {
			// In order to allow the ability to invoke multiple database drivers at the same
			// time, the DriverManager
			// class needs to be initialized before Class.forName is called. Calling this
			// particular method will invoke
			// the DriverManager so it is class loaded before trying to instantiate drivers.
			
			DriverManager.getLoginTimeout();
			return (Driver) Class.forName(className).newInstance();
		} catch (ClassNotFoundException e) {
			throw new ConnectorException("Failed Loading the class");
		} catch (InstantiationException | IllegalAccessException e) {
			throw new ConnectorException(e.getMessage());
		}
	}

	/**
	 * Method to test the Database Connection by taking the Standard JDBC
	 * Parameters.
	 *
	 * @throws ConnectorException the connector exception
	 */
	public void test() {
		try (Connection conn = getSoloConnection().connect(getUrl(), loadProperties());) {
			logger.log(Level.FINE, "Connection established successfully");
		} catch (SQLException e) {
			throw new ConnectorException(e.getMessage());
		}

	}
	/**
	 * Gets the custom property.
	 *
	 * @return the custom property
	 */
	public Map<String, String> getCustomProperty() {
		return customProperty;
	}

	/**
	 * Sets the custom property.
	 *
	 * @param customProperty the custom property
	 */
	public void setCustomProperty(Map<String, String> customProperty) {
		this.customProperty = customProperty;
	}
	
	public TransactableConnection connect() throws SQLException
	{
		_dbConnection=this.getCachedConnection();
		if (_dbConnection==null)
		{
			Connection dbConnection = this.getSoloConnection().connect(this.getUrl(), this.loadProperties());
			_dbConnection = new TransactableConnection(dbConnection);
		}
		return _dbConnection;
	}
	
	//Begin a transaction by putting a connection in the cache
	public void beginTransactionOperation() {
		if (this.isInTransaction())
			throw new ConnectorException("Process already has a pending transaction for connection: " + this.getUrl());
		try {
			TransactableConnection dbConnection=this.connect();
			ConcurrentMap<Object, Object> cache = this.getContext().getConnectorCache();
			String cacheKey = this.getCacheKey();
			cache.put(cacheKey, dbConnection);
			logger.info("Cache a connection: " + cacheKey);
			dbConnection.setAutoCommit(false);
			dbConnection.setIsInTransaction(true);
		} catch (SQLException e) {
			throw new ConnectorException(e);
		}	
	}

	public void commitOperation() {
		TransactableConnection dbConnection = null;
		if (!this.isInTransaction())
			throw new ConnectorException("Process execution has a no pending transaction for connection: " + this.getUrl());
		try {
			dbConnection = this.connect();
			this.clearConnectionCache();
			dbConnection.commitTransaction();
		} catch (SQLException e) {
			throw new ConnectorException(e);
		} finally {
			try {
				if (dbConnection!=null && !dbConnection.isClosed())
					dbConnection.closeTransaction();
			} catch (SQLException e) {
				throw new ConnectorException(e);
			}
		}
	}

	public void rollbackOperation() {
		TransactableConnection dbConnection = null;
		try {
			if (!this.isInTransaction())
				throw new ConnectorException("Process execution has a no pending transaction for connection: " + this.getUrl());
			dbConnection = this.connect();
			this.clearConnectionCache();
			dbConnection.rollback();
		} catch (SQLException e) {
			throw new ConnectorException(e);
		} finally {
			try {
				if (dbConnection!=null && !dbConnection.isClosed())
					dbConnection.closeTransaction();
			} catch (SQLException e) {
				throw new ConnectorException(e);
			}
		}
	}

	//We use existence of connection in the cache to indicate we are in a transaction
	public boolean isInTransaction()
	{
		return (this.getCachedConnection()!=null);
	}
	
	public TransactableConnection getCachedConnection()
	{
		String cacheKey = this.getCacheKey();
		ConcurrentMap<Object, Object> cache = this.getContext().getConnectorCache();
		if (cache==null)
			return null;
		TransactableConnection dbConnection = (TransactableConnection) cache.get(cacheKey);
		if (dbConnection!=null)
		{
			logger.info("Cached Connection Found");
			try {
				if(dbConnection.isClosed())
					throw new ConnectorException("Cached transaction connection is closed");
			} catch (SQLException e) {
				throw new ConnectorException("Error validating cached connection");
			}
		}
		return dbConnection;
	}
	
	private String getCacheKey()
	{
		String executionId=null;
   		if (ExecutionManager.getCurrent()!=null)
			executionId=ExecutionManager.getCurrent().getExecutionId();
   		//We base key on specific connections so we can have multiple pending transactions
   		//This useful when moving between 2 database connections
   		//TODO we will have problems if process has 2 connection instances with same url/username/password
		String cacheKey = "___CACHED_DB_CONNECTION_"+this.getUrl()+"_"+this.getUsername()+"_"+this.getPassword()+"_"+executionId;
		logger.info("cacheKey: " + cacheKey);
		return cacheKey;
	}
	
	//TODO if a process doesn't either commit or rollback, we may have a connection leak!!
	private void clearConnectionCache()
	{
		ConcurrentMap<Object, Object> cache = this.getContext().getConnectorCache();
		String cacheKey = this.getCacheKey();
		cache.remove(cacheKey);
	}
}