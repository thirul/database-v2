package com.boomi.connector.databaseconnector;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

public class TransactableConnection implements Connection{
	
	Connection _wrappedConnection;
	boolean _isInTransaction = false; //ignore close and commit if in transaction

	public TransactableConnection(Connection wrappedConnection)
	{
		this._wrappedConnection = wrappedConnection;
	}
	
	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		return _wrappedConnection.unwrap(iface);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return _wrappedConnection.isWrapperFor(iface);
	}

	@Override
	public Statement createStatement() throws SQLException {
		return _wrappedConnection.createStatement();
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		return _wrappedConnection.prepareStatement(sql);
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		return _wrappedConnection.prepareCall(sql);
	}

	@Override
	public String nativeSQL(String sql) throws SQLException {
		return _wrappedConnection.nativeSQL(sql);
	}

	@Override
	public void setAutoCommit(boolean autoCommit) throws SQLException {
		_wrappedConnection.setAutoCommit(autoCommit);
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		return _wrappedConnection.getAutoCommit();
	}

	@Override
	public void commit() throws SQLException {
		if (!_isInTransaction)
			_wrappedConnection.commit();
	}

	@Override
	public void rollback() throws SQLException {
		_wrappedConnection.rollback();
	}

	@Override
	public void close() throws SQLException {
		if (!_isInTransaction)
			_wrappedConnection.close();
	}

	@Override
	public boolean isClosed() throws SQLException {
		return _wrappedConnection.isClosed();
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		return _wrappedConnection.getMetaData();
	}

	@Override
	public void setReadOnly(boolean readOnly) throws SQLException {
		_wrappedConnection.setReadOnly(readOnly);
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		return _wrappedConnection.isReadOnly();
	}

	@Override
	public void setCatalog(String catalog) throws SQLException {
		_wrappedConnection.setCatalog(catalog);
	}

	@Override
	public String getCatalog() throws SQLException {
		return _wrappedConnection.getCatalog();
	}

	@Override
	public void setTransactionIsolation(int level) throws SQLException {
		_wrappedConnection.setTransactionIsolation(level);		
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		// TODO Auto-generated method stub
		return _wrappedConnection.getTransactionIsolation();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return _wrappedConnection.getWarnings();
	}

	@Override
	public void clearWarnings() throws SQLException {
		_wrappedConnection.clearWarnings();		
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency) throws SQLException {
		return _wrappedConnection.createStatement(resultSetType, resultSetConcurrency);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency)
			throws SQLException {
		return _wrappedConnection.prepareStatement(sql, resultSetType, resultSetConcurrency);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency) throws SQLException {
		return _wrappedConnection.prepareCall(sql, resultSetType, resultSetConcurrency);
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		return _wrappedConnection.getTypeMap();
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> map) throws SQLException {
		_wrappedConnection.setTypeMap(map);		
	}

	@Override
	public void setHoldability(int holdability) throws SQLException {
		_wrappedConnection.setHoldability(holdability);		
	}

	@Override
	public int getHoldability() throws SQLException {
		return _wrappedConnection.getHoldability();
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
		return _wrappedConnection.setSavepoint();
	}

	@Override
	public Savepoint setSavepoint(String name) throws SQLException {
		return _wrappedConnection.setSavepoint(name);
	}

	@Override
	public void rollback(Savepoint savepoint) throws SQLException {
		_wrappedConnection.rollback(savepoint);		
	}

	@Override
	public void releaseSavepoint(Savepoint savepoint) throws SQLException {
		_wrappedConnection.releaseSavepoint(savepoint);
	}

	@Override
	public Statement createStatement(int resultSetType, int resultSetConcurrency, int resultSetHoldability)
			throws SQLException {
		return _wrappedConnection.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		return _wrappedConnection.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public CallableStatement prepareCall(String sql, int resultSetType, int resultSetConcurrency,
			int resultSetHoldability) throws SQLException {
		return _wrappedConnection.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		return _wrappedConnection.prepareStatement(sql, autoGeneratedKeys);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int[] columnIndexes) throws SQLException {
		return _wrappedConnection.prepareStatement(sql, columnIndexes);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, String[] columnNames) throws SQLException {
		return _wrappedConnection.prepareStatement(sql, columnNames);
	}

	@Override
	public Clob createClob() throws SQLException {
		return _wrappedConnection.createClob();
	}

	@Override
	public Blob createBlob() throws SQLException {
		return _wrappedConnection.createBlob();
	}

	@Override
	public NClob createNClob() throws SQLException {
		return _wrappedConnection.createNClob();
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		return _wrappedConnection.createSQLXML();
	}

	@Override
	public boolean isValid(int timeout) throws SQLException {
		return _wrappedConnection.isValid(timeout);
	}

	@Override
	public void setClientInfo(String name, String value) throws SQLClientInfoException {
		_wrappedConnection.setClientInfo(name, value);	
	}

	@Override
	public void setClientInfo(Properties properties) throws SQLClientInfoException {
		_wrappedConnection.setClientInfo(properties);
	}

	@Override
	public String getClientInfo(String name) throws SQLException {
		return _wrappedConnection.getClientInfo(name);
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		return _wrappedConnection.getClientInfo();
	}

	@Override
	public Array createArrayOf(String typeName, Object[] elements) throws SQLException {
		return _wrappedConnection.createArrayOf(typeName, elements);
	}

	@Override
	public Struct createStruct(String typeName, Object[] attributes) throws SQLException {
		return _wrappedConnection.createStruct(typeName, attributes);
	}

	@Override
	public void setSchema(String schema) throws SQLException {
		_wrappedConnection.setSchema(schema);		
	}

	@Override
	public String getSchema() throws SQLException {
		return _wrappedConnection.getSchema();
	}

	@Override
	public void abort(Executor executor) throws SQLException {
		_wrappedConnection.abort(executor);		
	}

	@Override
	public void setNetworkTimeout(Executor executor, int milliseconds) throws SQLException {
		_wrappedConnection.setNetworkTimeout(executor, milliseconds);		
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		return _wrappedConnection.getNetworkTimeout();
	}
	
	public void closeTransaction() throws SQLException
	{
		this._wrappedConnection.close();
	}

	public void commitTransaction() throws SQLException
	{
		this._wrappedConnection.commit();
	}

	public boolean isInTransaction() {
		return _isInTransaction;
	}

	public void setIsInTransaction(boolean _isInTransaction) {
		this._isInTransaction = _isInTransaction;
	}
}
